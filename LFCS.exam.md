Below follows the core Domains and Competencies covered by the LFCS Exam.

# Command-line

Editing text files on the command line (echo, vi, nano, ...)
Manipulating text files from the command line (cat, sed, > ...)

# Filesystem & storage

Archiving and compressing files and directories (tar, gzip, xz, ... cpio)
/!\ Assembling partitions as LVM devices (vgcreate, ...)
    * physical volume pvcreate, pvmove, ...
    * volume group
    * logical volume
Configuring swap partitions (mkswap)
File attributes (chmod, chattr, lsattr, setfattr)
Finding files on the filesystem (find)
Formatting filesystems (mkfs)
Mounting filesystems automatically at boot time (/etc/fstab)
Mounting networked filesystems (cifs, smb, nfs) /!\ NFS
Partitioning storage devices (fdisk, parted, gdisk)
Troubleshooting filesystem issues (fsck, tune2fs, debugfs, dumpe2fs, badblocks)

# Local system administration

Creating backups (rsync, tar, cpio, dd, mt)
Creating local user groups (adduser, addgroup, useradd, groupadd)
Managing file permissions (chmod)
Managing fstab entries
Managing local users accounts (passwd)
/!\ Managing the startup process and related services (sysV, systemd, grub)
/!\ Managing user accounts (LDAP)
/!\ Managing user account attributes
Managing user processes (ps, top, fg, bg, htop)
Restoring backed up data
Setting file permissions and ownership (chmod, uid, gid, chown)

# Local security

Accessing the root account (su, sudo)
Using sudo to manage access to the root account (/etc/sudoers{.d})

# Shell scripting

Basic bash shell scripting (if for while read $() readlink bash zsh sh dash)

# Software management

/!\ Installing software packages (*rpm* dpkg apt *yum*)
